package classicgames.minesweeper.app.exception;

import static java.lang.String.format;

import java.util.UUID;

public class ApiKeyNotFoundException extends RuntimeException {

  private static final String APIKEY_MASK = "%s****-****-****-****-******%s";

  private static final long serialVersionUID = 1L;

  private static String maskApiKey(UUID apiKey) {
    if (apiKey == null) {
      return null;
    }

    final String apiKeyAsStr = apiKey.toString();

    return format(APIKEY_MASK, apiKeyAsStr.substring(0, 4), apiKeyAsStr.substring(30, 36));
  }

  public ApiKeyNotFoundException(UUID apiKey) {
    super(maskApiKey(apiKey));
  }
}
