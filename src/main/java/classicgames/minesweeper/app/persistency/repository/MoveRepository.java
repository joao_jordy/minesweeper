package classicgames.minesweeper.app.persistency.repository;

import classicgames.minesweeper.app.persistency.domain.GameEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface MoveRepository extends JpaRepository<GameEntity, Long> {}
