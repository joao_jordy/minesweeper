package classicgames.minesweeper.app.persistency.repository;

import classicgames.minesweeper.app.persistency.domain.UserEntity;
import java.util.Optional;
import java.util.UUID;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface UserRepository extends JpaRepository<UserEntity, Long> {

  Optional<UserEntity> findByApiKey(UUID requestApiKey);

  Optional<UserEntity> findByUsername(String username);
}
