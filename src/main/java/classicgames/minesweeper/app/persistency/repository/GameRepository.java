package classicgames.minesweeper.app.persistency.repository;

import classicgames.minesweeper.app.persistency.domain.GameEntity;
import java.util.Optional;
import java.util.Set;
import java.util.UUID;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface GameRepository extends JpaRepository<GameEntity, Long> {

  void deleteByExternalId(UUID gameId);

  Optional<GameEntity> findByUserIdAndExternalId(Long userId, UUID externalId);

  Set<GameEntity> findAllByUserId(Long userId);
}
