package classicgames.minesweeper.app.persistency.domain;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;

@Getter
@Setter
@Accessors(fluent = true)
@Entity(name = "POSITION")
public class PositionEntity {

  public enum Status {
    COVERED,
    UNCOVERED,
    FLAG,
    QUESTION,
    EXPLODED;
  }

  private int columnNumber;

  private boolean exploded;

  private boolean hasFlag;

  private boolean hasMine;

  private boolean hasQuestionMark;

  @Id @GeneratedValue private long id;

  private int rowNumber;

  private Status status;

  @Override
  public String toString() {
    return "PositionEntity [columnNumber="
        + columnNumber
        + ", exploded="
        + exploded
        + ", hasFlag="
        + hasFlag
        + ", hasMine="
        + hasMine
        + ", hasQuestionMark="
        + hasQuestionMark
        + ", id="
        + id
        + ", rowNumber="
        + rowNumber
        + ", status="
        + status
        + "]";
  }
}
