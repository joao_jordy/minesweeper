package classicgames.minesweeper.app.persistency.domain;

import java.time.Instant;
import java.util.Set;
import java.util.UUID;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;

@Getter
@Setter
@Accessors(fluent = true)
@Entity(name = "GAME")
public class GameEntity {

  public enum Result {
    LOST,
    WIN
  }

  public enum Status {
    CREATED,
    FINISHED,
    PAUSED,
    RUNNING
  }

  private int columns;

  private Instant created;

  private Long elapsedSeconds = 0L;

  private Instant finished;

  @Id @GeneratedValue private long id;

  private int mines;

  @OneToMany(cascade = CascadeType.PERSIST)
  private Set<PositionEntity> position;

  private Result result;

  private int rows;

  private Instant started;

  private Instant resumed;

  private Instant paused;

  private Status status;

  @ManyToOne private UserEntity user;

  private UUID externalId;
}
