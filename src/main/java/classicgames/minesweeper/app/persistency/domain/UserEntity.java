package classicgames.minesweeper.app.persistency.domain;

import static java.time.Instant.now;

import java.time.Instant;
import java.util.UUID;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;

@Getter
@Setter
@Accessors(fluent = true)
@Entity(name = "USER")
public class UserEntity {

  public enum Status {
    ACTIVE,
    INACTIVE;
  }

  private UUID apiKey;

  private Instant authExpiration;

  @Id @GeneratedValue private Long id;

  private String password;

  private String salt;

  private Status status;

  @Column(unique = true)
  private String username;

  private String email;

  public boolean isApiKeyExpired() {
    return now().isAfter(authExpiration);
  }
}
