package classicgames.minesweeper.app.port.in;

import java.util.UUID;

public interface AuthOperationPort {

  UUID login(String username, String password);

  void logout(UUID requestApiKey);
}
