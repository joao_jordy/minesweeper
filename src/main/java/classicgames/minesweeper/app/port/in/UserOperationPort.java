package classicgames.minesweeper.app.port.in;

import classicgames.minesweeper.app.adapter.rest.model.User;

public interface UserOperationPort {

  void createUser(String username, String password);

  void deleteLoggedUser();

  void updateLoggedUser(User body);
}
