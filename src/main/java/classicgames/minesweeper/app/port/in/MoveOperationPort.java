package classicgames.minesweeper.app.port.in;

import java.util.UUID;

public interface MoveOperationPort {

  void setQuestionMark(UUID gameId, Integer row, Integer column);

  void setRedFlag(UUID gameId, Integer row, Integer column);

  void unsetQuestionMark(UUID gameId, Integer row, Integer column);

  void unsetRedFlag(UUID gameId, Integer row, Integer column);
}
