package classicgames.minesweeper.app.port.in;

import classicgames.minesweeper.app.persistency.domain.GameEntity;
import java.util.Optional;
import java.util.Set;
import java.util.UUID;

public interface GameQueryPort {

  Set<GameEntity> findAll();

  Optional<GameEntity> findByExternalId(UUID gameId);
}
