package classicgames.minesweeper.app.port.in;

import classicgames.minesweeper.app.adapter.rest.model.User;

public interface UserQueryPort {

  User getLoggedUser();
}
