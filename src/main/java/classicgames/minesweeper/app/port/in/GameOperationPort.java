package classicgames.minesweeper.app.port.in;

import classicgames.minesweeper.app.persistency.domain.GameEntity;
import java.util.UUID;

public interface GameOperationPort {

  GameEntity create(int rows, int columns, int mines);

  void delete(UUID gameId);

  void pause(UUID gameId);

  void resume(UUID gameId);

  void start(UUID gameId);
}
