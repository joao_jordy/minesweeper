package classicgames.minesweeper.app.port.in;

import java.util.UUID;

public interface MoveTurnPositionPort {

  void turnPosition(UUID gameId, Integer row, Integer column);
}
