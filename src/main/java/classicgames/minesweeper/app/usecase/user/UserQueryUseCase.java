package classicgames.minesweeper.app.usecase.user;

import classicgames.minesweeper.app.adapter.parser.UserParser;
import classicgames.minesweeper.app.adapter.rest.model.User;
import classicgames.minesweeper.app.adapter.tenant.TenantAdapter;
import classicgames.minesweeper.app.persistency.repository.UserRepository;
import classicgames.minesweeper.app.port.in.UserQueryPort;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class UserQueryUseCase implements UserQueryPort {

  @Autowired private UserParser parser;

  @Autowired private UserRepository repository;

  @Autowired private TenantAdapter tenant;

  @Override
  public User getLoggedUser() {
    final var user = repository.findById(tenant.getCurrentTenant().userId());

    return parser.parse(user.get());
  }
}
