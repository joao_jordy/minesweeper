package classicgames.minesweeper.app.usecase.user;

import classicgames.minesweeper.app.adapter.rest.model.User;
import classicgames.minesweeper.app.adapter.tenant.TenantAdapter;
import classicgames.minesweeper.app.persistency.domain.UserEntity;
import classicgames.minesweeper.app.persistency.domain.UserEntity.Status;
import classicgames.minesweeper.app.persistency.repository.UserRepository;
import classicgames.minesweeper.app.port.in.UserOperationPort;
import java.util.UUID;
import org.apache.commons.codec.digest.DigestUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class UserOperationUseCase implements UserOperationPort {

  @Autowired private TenantAdapter tenant;

  @Autowired UserRepository repository;

  @Override
  public void createUser(String username, String password) {
    final var salt = UUID.randomUUID().toString();
    final var sha3Hex = new DigestUtils("SHA3-256").digestAsHex(password + salt);

    repository.save(
        new UserEntity().username(username).password(sha3Hex).salt(salt).status(Status.ACTIVE));
  }

  @Override
  public void deleteLoggedUser() {
    final var user = repository.findById(tenant.getCurrentTenant().userId());

    if (user.isPresent()) {
      repository.delete(user.get());
    }
  }

  @Override
  public void updateLoggedUser(User body) {
    final var user = repository.findById(tenant.getCurrentTenant().userId());

    if (user.isPresent()) {
      user.get().username(body.getUsername()).password(body.getPassword());
      repository.save(user.get());
    }
  }
}
