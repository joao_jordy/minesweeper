package classicgames.minesweeper.app.usecase.auth;

import static java.time.Instant.now;
import static java.util.UUID.randomUUID;

import classicgames.minesweeper.app.adapter.rest.auth.AuthHashParser;
import classicgames.minesweeper.app.exception.ApiKeyNotFoundException;
import classicgames.minesweeper.app.exception.UserNotFoundException;
import classicgames.minesweeper.app.persistency.domain.UserEntity;
import classicgames.minesweeper.app.persistency.repository.UserRepository;
import classicgames.minesweeper.app.port.in.AuthOperationPort;
import java.util.UUID;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class AuthOperationUseCase implements AuthOperationPort {

  private static final int ONE_HOUR_IN_SECONDS = 60 * 60;

  @Autowired AuthHashParser hashParser;

  @Autowired UserRepository repository;

  @Override
  public UUID login(String username, String password) {
    final var user = findUser(username);

    if (checkCredentials(password, user)) {
      renewAuthentication(user);
    }

    return user.apiKey();
  }

  @Override
  public void logout(UUID requestApiKey) {
    final var user = findUser(requestApiKey);

    repository.save(user.authExpiration(now()));
  }

  private boolean checkCredentials(String password, UserEntity user) {
    return user.password().equals(hashParser.parse(password, user.salt()));
  }

  private UserEntity findUser(String username) {
    final var user = repository.findByUsername(username);

    if (user.isEmpty()) {
      throw new UserNotFoundException(username);
    }

    if (!UserEntity.Status.ACTIVE.equals(user.get().status())) {
      throw new UserNotFoundException(username);
    }

    return user.get();
  }

  private UserEntity findUser(UUID requestApiKey) {
    final var user = repository.findByApiKey(requestApiKey);

    if (user.isEmpty()) {
      throw new ApiKeyNotFoundException(requestApiKey);
    }

    return user.get();
  }

  private void renewAuthentication(final UserEntity user) {
    repository.save(
        user.apiKey(randomUUID()).authExpiration(now().plusSeconds(ONE_HOUR_IN_SECONDS)));
  }
}
