package classicgames.minesweeper.app.usecase.move;

import static java.lang.Math.max;
import static java.lang.Math.min;

import classicgames.minesweeper.app.adapter.tenant.TenantAdapter;
import classicgames.minesweeper.app.exception.GameNotFoundException;
import classicgames.minesweeper.app.persistency.domain.GameEntity;
import classicgames.minesweeper.app.persistency.domain.PositionEntity;
import classicgames.minesweeper.app.persistency.repository.GameRepository;
import classicgames.minesweeper.app.port.in.MoveTurnPositionPort;
import java.time.Instant;
import java.util.Optional;
import java.util.UUID;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class MoveTurnPositionUseCase implements MoveTurnPositionPort {

  @Autowired GameRepository repository;

  @Autowired TenantAdapter tenant;

  @Override
  public void turnPosition(UUID gameId, Integer row, Integer column) {
    final var game = findGame(gameId);

    final var position = findPosition(game, row, column);

    if (position.get().hasMine()) {
      mineExplosion(game, position.get());
    } else {

      if (position.isPresent() && PositionEntity.Status.COVERED.equals(position.get().status())) {
        position.get().status(PositionEntity.Status.UNCOVERED);
        repository.save(game);
      }

      uncoverAllBoundaries(game, position.get());

      checkGameComplete(game);
    }
  }

  private void checkGameComplete(GameEntity game) {
    final var uncoveredTotal =
        game.position()
            .parallelStream()
            .filter(p -> !p.hasMine() && PositionEntity.Status.UNCOVERED.equals(p.status()))
            .count();

    if (uncoveredTotal == game.rows() * game.columns() - game.mines()) {
      finishGame(game);
    }
  }

  private GameEntity findGame(UUID gameId) {
    final var game =
        repository.findByUserIdAndExternalId(tenant.getCurrentTenant().userId(), gameId);

    if (game.isEmpty() || !GameEntity.Status.RUNNING.equals(game.get().status())) {
      throw new GameNotFoundException(gameId);
    }

    return game.get();
  }

  private Optional<PositionEntity> findPosition(GameEntity game, Integer row, Integer column) {
    return game.position()
        .parallelStream()
        .filter(p -> p.rowNumber() == row && p.columnNumber() == column)
        .findFirst();
  }

  private void finishGame(GameEntity game) {
    final var endTime = Instant.now();
    final var elapsedSeconds = endTime.getEpochSecond() - game.resumed().getEpochSecond();

    uncoverAllMines(game);

    repository.save(
        game //
            .status(GameEntity.Status.FINISHED) //
            .finished(endTime) //
            .elapsedSeconds(elapsedSeconds));
  }

  private boolean hasMinesInTheBoundaries(GameEntity game, PositionEntity pe) {
    for (int i = max(pe.rowNumber() - 1, 0); i < min(pe.rowNumber() + 1, game.rows()); i++) {
      for (int j = max(pe.columnNumber() - 1, 0); j < min(pe.rowNumber() + 1, game.rows()); j++) {
        if (!findPosition(game, i, j).get().hasMine()) {
          return true;
        }
      }
    }

    return false;
  }

  private void mineExplosion(GameEntity game, PositionEntity position) {
    position.status(PositionEntity.Status.EXPLODED);

    finishGame(game);
  }

  private void uncoverAllBoundaries(GameEntity game, PositionEntity position) {
    if (hasMinesInTheBoundaries(game, position)) {
      return;
    }

    uncoverBoundaries(game, position);
  }

  private void uncoverAllMines(GameEntity game) {
    game.position()
        .parallelStream()
        .filter(p -> p.hasMine())
        .forEach(p -> p.status(PositionEntity.Status.UNCOVERED));
  }

  private void uncoverBoundaries(GameEntity game, PositionEntity pe) {
    for (var i = max(pe.rowNumber() - 1, 0); i < min(pe.rowNumber() + 1, game.rows()); i++) {
      for (var j = max(pe.columnNumber() - 1, 0); j < min(pe.rowNumber() + 1, game.rows()); j++) {
        pe.status(PositionEntity.Status.UNCOVERED);

        uncoverAllBoundaries(game, pe);
      }
    }
  }
}
