package classicgames.minesweeper.app.usecase.move;

import classicgames.minesweeper.app.adapter.tenant.TenantAdapter;
import classicgames.minesweeper.app.exception.GameNotFoundException;
import classicgames.minesweeper.app.persistency.domain.GameEntity;
import classicgames.minesweeper.app.persistency.domain.PositionEntity;
import classicgames.minesweeper.app.persistency.repository.GameRepository;
import classicgames.minesweeper.app.port.in.MoveOperationPort;
import java.util.Optional;
import java.util.UUID;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class MoveOperationUseCase implements MoveOperationPort {

  @Autowired GameRepository repository;

  @Autowired TenantAdapter tenant;

  @Override
  public void setQuestionMark(UUID gameId, Integer row, Integer column) {
    final var game = findGame(gameId);

    final var position = findPosition(game, row, column);

    if (position.isPresent() && PositionEntity.Status.COVERED.equals(position.get().status())) {
      position.get().status(PositionEntity.Status.QUESTION);
      repository.save(game);
    }
  }

  @Override
  public void setRedFlag(UUID gameId, Integer row, Integer column) {
    final var game = findGame(gameId);

    final var position = findPosition(game, row, column);

    if (position.isPresent() && PositionEntity.Status.COVERED.equals(position.get().status())) {
      position.get().status(PositionEntity.Status.FLAG);
      repository.save(game);
    }
  }

  @Override
  public void unsetQuestionMark(UUID gameId, Integer row, Integer column) {
    final var game = findGame(gameId);

    final var position = findPosition(game, row, column);

    if (position.isPresent() && PositionEntity.Status.QUESTION.equals(position.get().status())) {
      position.get().status(PositionEntity.Status.COVERED);
      repository.save(game);
    }
  }

  @Override
  public void unsetRedFlag(UUID gameId, Integer row, Integer column) {
    final var game = findGame(gameId);

    final var position = findPosition(game, row, column);

    if (position.isPresent() && PositionEntity.Status.FLAG.equals(position.get().status())) {
      position.get().status(PositionEntity.Status.COVERED);
      repository.save(game);
    }
  }

  private GameEntity findGame(UUID gameId) {
    final var game =
        repository.findByUserIdAndExternalId(tenant.getCurrentTenant().userId(), gameId);

    if (game.isEmpty() || !GameEntity.Status.RUNNING.equals(game.get().status())) {
      throw new GameNotFoundException(gameId);
    }

    return game.get();
  }

  private Optional<PositionEntity> findPosition(GameEntity game, Integer row, Integer column) {
    return game.position()
        .parallelStream()
        .filter(p -> p.rowNumber() == row && p.columnNumber() == column)
        .findFirst();
  }
}
