package classicgames.minesweeper.app.usecase.game;

import classicgames.minesweeper.app.adapter.builder.NewGameBuilder;
import classicgames.minesweeper.app.adapter.tenant.TenantAdapter;
import classicgames.minesweeper.app.persistency.domain.GameEntity;
import classicgames.minesweeper.app.persistency.repository.GameRepository;
import classicgames.minesweeper.app.port.in.GameQueryPort;
import java.util.Optional;
import java.util.Set;
import java.util.UUID;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class GameQueryUseCase implements GameQueryPort {

  @Autowired NewGameBuilder builder;

  @Autowired GameRepository repository;

  @Autowired TenantAdapter tenant;

  @Override
  public Set<GameEntity> findAll() {
    return repository.findAllByUserId(tenant.getCurrentTenant().userId());
  }

  @Override
  public Optional<GameEntity> findByExternalId(UUID gameId) {
    return repository.findByUserIdAndExternalId(tenant.getCurrentTenant().userId(), gameId);
  }
}
