package classicgames.minesweeper.app.usecase.game;

import static classicgames.minesweeper.app.persistency.domain.GameEntity.Status.PAUSED;
import static classicgames.minesweeper.app.persistency.domain.GameEntity.Status.RUNNING;

import classicgames.minesweeper.app.adapter.builder.NewGameBuilder;
import classicgames.minesweeper.app.adapter.tenant.TenantAdapter;
import classicgames.minesweeper.app.persistency.domain.GameEntity;
import classicgames.minesweeper.app.persistency.domain.GameEntity.Status;
import classicgames.minesweeper.app.persistency.repository.GameRepository;
import classicgames.minesweeper.app.port.in.GameOperationPort;
import java.time.Instant;
import java.util.UUID;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class GameOperationUseCase implements GameOperationPort {

  @Autowired NewGameBuilder builder;

  @Autowired GameRepository repository;

  @Autowired TenantAdapter tenant;

  @Override
  public GameEntity create(int rows, int columns, int mines) {
    final var gameEntity = builder.build(rows, columns, mines, tenant.getCurrentTenant().userId());

    return repository.save(gameEntity);
  }

  @Override
  public void delete(UUID gameId) {
    final var game =
        repository.findByUserIdAndExternalId(tenant.getCurrentTenant().userId(), gameId);

    if (game.isPresent()) {
      repository.delete(game.get());
    }
  }

  @Override
  public void pause(UUID gameId) {
    final var game =
        repository.findByUserIdAndExternalId(tenant.getCurrentTenant().userId(), gameId);

    if (game.isPresent() && RUNNING.equals(game.get().status())) {
      final var paused = Instant.now();
      final var elapsedSeconds = paused.getEpochSecond() - game.get().resumed().getEpochSecond();

      repository.save(
          game.get() //
              .status(PAUSED) //
              .paused(paused) //
              .elapsedSeconds(elapsedSeconds));
    }
  }

  @Override
  public void resume(UUID gameId) {
    final var game =
        repository.findByUserIdAndExternalId(tenant.getCurrentTenant().userId(), gameId);

    if (game.isPresent() && PAUSED.equals(game.get().status())) {
      repository.save(game.get().status(RUNNING).resumed(Instant.now()));
    }
  }

  @Override
  public void start(UUID gameId) {
    final var game =
        repository.findByUserIdAndExternalId(tenant.getCurrentTenant().userId(), gameId);

    if (game.isPresent() && Status.CREATED.equals(game.get().status())) {
      repository.save(game.get().status(RUNNING).started(Instant.now()).resumed(Instant.now()));
    }
  }
}
