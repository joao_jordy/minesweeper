package classicgames.minesweeper.app.adapter.tenant;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.UNAUTHORIZED)
public class InvalidTenantConfigurationException extends RuntimeException {

  private static final long serialVersionUID = 1L;

  public InvalidTenantConfigurationException(String message) {
    super(message);
  }
}
