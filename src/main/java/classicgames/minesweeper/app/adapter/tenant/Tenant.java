package classicgames.minesweeper.app.adapter.tenant;

import java.util.UUID;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;

@Accessors(fluent = true)
@Getter
@Setter
public class Tenant {

  private UUID apiKey;

  private long userId;
}
