package classicgames.minesweeper.app.adapter.tenant;

import static java.util.Optional.of;

import classicgames.minesweeper.app.persistency.repository.UserRepository;
import java.util.Optional;
import java.util.UUID;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class TenantAdapter {

  private ThreadLocal<Tenant> threadLocal;

  @Autowired UserRepository repository;

  public TenantAdapter() {
    threadLocal = new ThreadLocal<>();
  }

  public void clearCurrentTenant() {
    synchronized (this) {
      threadLocal.remove();
    }
  }

  public Optional<Tenant> configureTenant(UUID requestApiKey) {
    final var user = repository.findByApiKey(requestApiKey);

    if (user.isPresent() && !user.get().isApiKeyExpired()) {
      return of(buildAndSetTenant(requestApiKey, user.get().id()));
    }

    throw new InvalidTenantConfigurationException("Tenant nao configurado");
  }

  public Tenant getCurrentTenant() {
    synchronized (this) {
      if (threadLocal.get() != null) {
        return threadLocal.get();
      }
    }

    throw new InvalidTenantConfigurationException("Tenant nao configurado");
  }

  private Tenant buildAndSetTenant(UUID requestApiKey, final long userId) {
    final var tenant = new Tenant().apiKey(requestApiKey).userId(userId);

    synchronized (this) {
      threadLocal.set(tenant);
    }

    return tenant;
  }
}
