package classicgames.minesweeper.app.adapter.parser;

import classicgames.minesweeper.app.adapter.rest.model.User;
import classicgames.minesweeper.app.persistency.domain.UserEntity;
import org.springframework.stereotype.Service;

@Service
public class UserParser {

  public User parse(UserEntity userEntity) {
    return new User().username(userEntity.username()).email(userEntity.email());
  }
}
