package classicgames.minesweeper.app.adapter.parser;

import static classicgames.minesweeper.app.adapter.rest.model.Position.StatusEnum.valueOf;

import classicgames.minesweeper.app.adapter.rest.model.Game;
import classicgames.minesweeper.app.adapter.rest.model.Game.StatusEnum;
import classicgames.minesweeper.app.adapter.rest.model.Position;
import classicgames.minesweeper.app.persistency.domain.GameEntity;
import classicgames.minesweeper.app.persistency.domain.PositionEntity;
import org.springframework.stereotype.Component;

@Component
public class GameApiParser {

  public Game parse(GameEntity entity) {
    final var game =
        new Game() //
            .rows(entity.rows()) //
            .columns(entity.columns()) //
            .mines(entity.mines()) //
            .status(StatusEnum.valueOf(entity.status().name())) //
            .elapsedSeconds(entity.elapsedSeconds()) //
            .finished(entity.finished()) //
            .started(entity.started()) //
            .created(entity.created()) //
            .id(entity.externalId());

    parsePositions(entity, game);

    return game;
  }

  private void parsePositions(GameEntity entity, Game game) {
    for (PositionEntity pe : entity.position()) {
      game.addPositionsItem(
          new Position() //
              .row(pe.rowNumber()) //
              .column(pe.columnNumber()) //
              .status(valueOf(pe.status().name())) //
              .mine(pe.hasMine())); //
    }
  }
}
