package classicgames.minesweeper.app.adapter.rest.auth;

import static java.lang.String.format;

import org.apache.commons.codec.digest.DigestUtils;
import org.springframework.stereotype.Service;

@Service
public class AuthHashParser {

  private static final String ALGORITHM = "SHA3-256";

  private static final DigestUtils DIGEST_UTILS = new DigestUtils(ALGORITHM);

  public String parse(String password, String salt) {
    return DIGEST_UTILS.digestAsHex(format("%s%s", password, salt));
  }
}
