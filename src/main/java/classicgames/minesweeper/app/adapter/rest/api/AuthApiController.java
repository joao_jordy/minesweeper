package classicgames.minesweeper.app.adapter.rest.api;

import classicgames.minesweeper.app.adapter.rest.model.Login;
import classicgames.minesweeper.app.adapter.tenant.TenantAdapter;
import classicgames.minesweeper.app.port.in.AuthOperationPort;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import io.swagger.annotations.Authorization;
import java.util.UUID;
import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@javax.annotation.Generated(
    value = "io.swagger.codegen.languages.SpringCodegen",
    date = "2021-06-01T02:52:45.874Z")
@Controller
@RequestMapping(value = "/")
public class AuthApiController implements AuthApi {

  @Autowired AuthOperationPort authOperationPort;

  @Autowired TenantAdapter tenantAdapter;

  // @formatter:off
  @ApiOperation(
      value = "Logs user into the system",
      nickname = "loginUser",
      notes = "",
      response = Object.class,
      tags = {
        "Auth",
      })
  @ApiResponses(
      value = {
        @ApiResponse(code = 200, message = "successful operation", response = Object.class),
        @ApiResponse(code = 400, message = "Invalid username/password supplied")
      })
  @RequestMapping(
      value = "/login",
      produces = {"application/json"},
      consumes = {"application/json"},
      method = RequestMethod.PUT) // @formatter:on
  public ResponseEntity<UUID> loginUser(
      @ApiParam(value = "Created user object", required = true) @Valid @RequestBody Login body) {

    final var apiKey = authOperationPort.login(body.getUsername(), body.getPassword());

    return ResponseEntity.ok(apiKey);
  }

  // @formatter:off
  @ApiOperation(
      value = "Logs out current logged in user session",
      nickname = "logoutUser",
      notes = "",
      authorizations = {@Authorization(value = "api_key")},
      tags = {
        "Auth",
      })
  @ApiResponses(value = {@ApiResponse(code = 200, message = "successful operation")})
  @RequestMapping(
      value = "/logout",
      produces = {"application/json"},
      consumes = {"application/json"},
      method = RequestMethod.PUT) // @formatter:on
  public ResponseEntity<Void> logoutUser() {
    authOperationPort.logout(tenantAdapter.getCurrentTenant().apiKey());

    return ResponseEntity.ok().build();
  }
}
