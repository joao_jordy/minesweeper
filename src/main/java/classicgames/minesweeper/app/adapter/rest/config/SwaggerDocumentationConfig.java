package classicgames.minesweeper.app.adapter.rest.config;

import java.util.Arrays;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.ParameterBuilder;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.schema.ModelRef;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Contact;
import springfox.documentation.service.Parameter;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;

@javax.annotation.Generated(
    value = "io.swagger.codegen.languages.SpringCodegen",
    date = "2021-05-31T01:09:08.889Z")
@Configuration
public class SwaggerDocumentationConfig {

  @Bean
  public Docket customImplementation() {
    return new Docket(DocumentationType.SWAGGER_2)
        .select()
        .apis(RequestHandlerSelectors.basePackage("classicgames.minesweeper.app.adapter.rest.api"))
        .build()
        .directModelSubstitute(java.time.LocalDate.class, java.sql.Date.class)
        .directModelSubstitute(java.time.OffsetDateTime.class, java.util.Date.class)
        .globalOperationParameters(Arrays.asList(new Parameter[] {apiKey(), contentType()}))
        .apiInfo(apiInfo());
  }

  private Parameter apiKey() {
    return new ParameterBuilder()
        .name("X-API-KEY")
        .modelRef(new ModelRef("string"))
        .parameterType("header")
        .required(true)
        .build();
  }

  private Parameter contentType() {
    return new ParameterBuilder()
        .name("Content-Type")
        .modelRef(new ModelRef("string"))
        .parameterType("header")
        .allowMultiple(false)
        .required(false)
        .defaultValue("application/json")
        .build();
  }

  ApiInfo apiInfo() {
    return new ApiInfoBuilder()
        .title("Minesweeper")
        .description("Minesweeper Classic Game API")
        .license("")
        .licenseUrl("http://unlicense.org")
        .termsOfServiceUrl("")
        .version("1.0.0")
        .contact(new Contact("", "", ""))
        .build();
  }
}
