package classicgames.minesweeper.app.adapter.rest.api;

import static java.util.stream.Collectors.toSet;

import classicgames.minesweeper.app.adapter.parser.GameApiParser;
import classicgames.minesweeper.app.adapter.rest.model.Game;
import classicgames.minesweeper.app.persistency.domain.GameEntity;
import classicgames.minesweeper.app.port.in.GameOperationPort;
import classicgames.minesweeper.app.port.in.GameQueryPort;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import io.swagger.annotations.Authorization;
import io.swagger.annotations.AuthorizationScope;
import java.util.Optional;
import java.util.UUID;
import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@javax.annotation.Generated(
    value = "io.swagger.codegen.languages.SpringCodegen",
    date = "2021-05-31T01:09:08.889Z")
@Controller
@RequestMapping(value = "/")
public class GameApiController implements GameApi {

  @Autowired GameOperationPort gameOperationPort;

  @Autowired GameQueryPort gameQueryPort;

  @Autowired GameApiParser parser;

  // @formatter:off
  @ApiOperation(
      value = "Add a new game to the user",
      nickname = "addGame",
      notes = "",
      authorizations = {
        @Authorization(
            value = "minesweeper_auth",
            scopes = {
              @AuthorizationScope(
                  scope = "write:games",
                  description = "modify games in your account"),
              @AuthorizationScope(scope = "read:games", description = "read your games")
            })
      },
      tags = {
        "Game",
      })
  @ApiResponses(value = {@ApiResponse(code = 405, message = "Invalid input")})
  @RequestMapping(
      value = "/game",
      produces = {"application/json"},
      consumes = {"application/json"},
      method = RequestMethod.POST) // @formatter:on
  public ResponseEntity<Game> addGame(
      @ApiParam(value = "Game object that needs to be added to the user", required = true)
          @Valid
          @RequestBody
          Game body) {

    final var gameEntity =
        gameOperationPort.create(body.getRows(), body.getColumns(), body.getMines());

    final Game game = parser.parse(gameEntity);

    return ResponseEntity.ok(game);
  }

  // @formatter:off
  @ApiOperation(
      value = "Deletes a game",
      nickname = "deleteGame",
      notes = "",
      authorizations = {@Authorization(value = "api_key")},
      tags = {
        "Game",
      })
  @ApiResponses(
      value = {
        @ApiResponse(code = 400, message = "Invalid ID supplied"),
        @ApiResponse(code = 404, message = "Game not found")
      })
  @RequestMapping(
      value = "/game/{game_id}",
      produces = {"application/json"},
      consumes = {"application/json"},
      method = RequestMethod.DELETE) // @formatter:on
  public ResponseEntity<Void> deleteGame(
      @ApiParam(value = "Game id to delete", required = true) @PathVariable("game_id") UUID gameId,
      @ApiParam(value = "") @RequestHeader(value = "api_key", required = false) String apiKey) {
    gameOperationPort.delete(gameId);

    return ResponseEntity.ok().build();
  }

  // @formatter:off
  @ApiOperation(
      value = "Find game by ID",
      nickname = "getGameById",
      notes = "Returns a single game",
      response = Game.class,
      authorizations = {@Authorization(value = "api_key")},
      tags = {
        "Game",
      })
  @ApiResponses(
      value = {
        @ApiResponse(code = 200, message = "successful operation", response = Game.class),
        @ApiResponse(code = 400, message = "Invalid ID supplied"),
        @ApiResponse(code = 404, message = "Game not found")
      })
  @RequestMapping(
      value = "/game/{game_id}",
      produces = {"application/json"},
      consumes = {"application/json"},
      method = RequestMethod.GET) // @formatter:on
  public ResponseEntity<Game> getGameById(
      @ApiParam(value = "ID of game to return", required = true) @PathVariable("game_id")
          UUID gameId) {
    final Optional<GameEntity> gameEntity = gameQueryPort.findByExternalId(gameId);

    if (!gameEntity.isPresent()) {
      return ResponseEntity.notFound().build();
    }

    return ResponseEntity.ok(parser.parse(gameEntity.get()));
  }

  // @formatter:off
  @ApiOperation(
      value = "Find game by ID",
      nickname = "getGames",
      notes = "Returns a single game",
      response = Object.class,
      authorizations = {@Authorization(value = "api_key")},
      tags = {
        "Game",
      })
  @ApiResponses(
      value = {
        @ApiResponse(code = 200, message = "successful operation", response = Object.class),
        @ApiResponse(code = 400, message = "Invalid ID supplied"),
        @ApiResponse(code = 404, message = "Game not found")
      })
  @RequestMapping(
      value = "/game",
      produces = {"application/json"},
      consumes = {"application/json"},
      method = RequestMethod.GET) // @formatter:on
  public ResponseEntity<Object> getGames() {
    final var games = gameQueryPort.findAll();

    final var gamesSet = games.parallelStream().map(g -> parser.parse(g)).collect(toSet());

    return ResponseEntity.ok(gamesSet);
  }

  // @formatter:off
  @ApiOperation(
      value = "Pause a game by ID",
      nickname = "pauseGameById",
      notes = "Pause a single game",
      authorizations = {@Authorization(value = "api_key")},
      tags = {
        "Game",
      })
  @ApiResponses(
      value = {
        @ApiResponse(code = 200, message = "successful operation"),
        @ApiResponse(code = 400, message = "Invalid ID supplied"),
        @ApiResponse(code = 404, message = "Game not found")
      })
  @RequestMapping(
      value = "/game/{game_id}/pause",
      produces = {"application/json"},
      consumes = {"application/json"},
      method = RequestMethod.PUT) // @formatter:on
  public ResponseEntity<Void> pauseGameById(
      @ApiParam(value = "ID of game to return", required = true) @PathVariable("game_id")
          UUID gameId) {

    gameOperationPort.pause(gameId);

    return ResponseEntity.ok().build();
  }

  // @formatter:off
  @ApiOperation(
      value = "Resume a game by ID",
      nickname = "resumeGameById",
      notes = "Resume a single game",
      authorizations = {@Authorization(value = "api_key")},
      tags = {
        "Game",
      })
  @ApiResponses(
      value = {
        @ApiResponse(code = 200, message = "successful operation"),
        @ApiResponse(code = 400, message = "Invalid ID supplied"),
        @ApiResponse(code = 404, message = "Game not found")
      })
  @RequestMapping(
      value = "/game/{game_id}/resume",
      produces = {"application/json"},
      consumes = {"application/json"},
      method = RequestMethod.PUT) // @formatter:on
  public ResponseEntity<Void> resumeGameById(
      @ApiParam(value = "ID of game to return", required = true) @PathVariable("game_id")
          UUID gameId) {

    gameOperationPort.resume(gameId);

    return ResponseEntity.ok().build();
  }

  // @formatter:off
  @ApiOperation(
      value = "Start a game by ID",
      nickname = "startGameById",
      notes = "Start a single game",
      authorizations = {@Authorization(value = "api_key")},
      tags = {
        "Game",
      })
  @ApiResponses(
      value = {
        @ApiResponse(code = 200, message = "successful operation"),
        @ApiResponse(code = 400, message = "Invalid ID supplied"),
        @ApiResponse(code = 404, message = "Game not found")
      })
  @RequestMapping(
      value = "/game/{game_id}/start",
      produces = {"application/json"},
      consumes = {"application/json"},
      method = RequestMethod.PUT) // @formatter:on
  public ResponseEntity<Void> startGameById(
      @ApiParam(value = "ID of game to return", required = true) @PathVariable("game_id")
          UUID gameId) {

    gameOperationPort.start(gameId);

    return ResponseEntity.ok().build();
  }
}
