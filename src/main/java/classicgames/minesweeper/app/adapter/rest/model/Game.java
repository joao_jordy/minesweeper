package classicgames.minesweeper.app.adapter.rest.model;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonValue;
import io.swagger.annotations.ApiModelProperty;
import java.time.Instant;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.UUID;
import javax.validation.Valid;
import javax.validation.constraints.*;
import org.springframework.validation.annotation.Validated;

/** Game */
@Validated
@javax.annotation.Generated(
    value = "io.swagger.codegen.languages.SpringCodegen",
    date = "2021-06-03T02:48:29.920Z")
public class Game {
  @JsonProperty("id")
  private UUID id = null;

  @JsonProperty("rows")
  private Integer rows = null;

  @JsonProperty("columns")
  private Integer columns = null;

  @JsonProperty("mines")
  private Integer mines = null;

  /** game status */
  public enum StatusEnum {
    CREATED("CREATED"),

    PAUSED("PAUSED"),

    RUNNING("RUNNING"),

    FINISHED("FINISHED");

    private String value;

    StatusEnum(String value) {
      this.value = value;
    }

    @Override
    @JsonValue
    public String toString() {
      return String.valueOf(value);
    }

    @JsonCreator
    public static StatusEnum fromValue(String text) {
      for (StatusEnum b : StatusEnum.values()) {
        if (String.valueOf(b.value).equals(text)) {
          return b;
        }
      }
      return null;
    }
  }

  @JsonProperty("status")
  private StatusEnum status = null;

  @JsonProperty("created")
  private Instant created = null;

  @JsonProperty("started")
  private Instant started = null;

  @JsonProperty("finished")
  private Instant finished = null;

  @JsonProperty("elapsed_seconds")
  private Long elapsedSeconds = null;

  @JsonProperty("positions")
  @Valid
  private List<Position> positions = null;

  public Game id(UUID id) {
    this.id = id;
    return this;
  }

  /**
   * Get id
   *
   * @return id
   */
  @ApiModelProperty(readOnly = true, value = "")
  @Valid
  public UUID getId() {
    return id;
  }

  public void setId(UUID id) {
    this.id = id;
  }

  public Game rows(Integer rows) {
    this.rows = rows;
    return this;
  }

  /**
   * Get rows minimum: 3
   *
   * @return rows
   */
  @ApiModelProperty(value = "")
  @Min(3)
  public Integer getRows() {
    return rows;
  }

  public void setRows(Integer rows) {
    this.rows = rows;
  }

  public Game columns(Integer columns) {
    this.columns = columns;
    return this;
  }

  /**
   * Get columns minimum: 3
   *
   * @return columns
   */
  @ApiModelProperty(value = "")
  @Min(3)
  public Integer getColumns() {
    return columns;
  }

  public void setColumns(Integer columns) {
    this.columns = columns;
  }

  public Game mines(Integer mines) {
    this.mines = mines;
    return this;
  }

  /**
   * Get mines minimum: 1
   *
   * @return mines
   */
  @ApiModelProperty(value = "")
  @Min(1)
  public Integer getMines() {
    return mines;
  }

  public void setMines(Integer mines) {
    this.mines = mines;
  }

  public Game status(StatusEnum status) {
    this.status = status;
    return this;
  }

  /**
   * game status
   *
   * @return status
   */
  @ApiModelProperty(readOnly = true, value = "game status")
  public StatusEnum getStatus() {
    return status;
  }

  public void setStatus(StatusEnum status) {
    this.status = status;
  }

  public Game created(Instant created) {
    this.created = created;
    return this;
  }

  /**
   * Get created
   *
   * @return created
   */
  @ApiModelProperty(readOnly = true, value = "")
  @Valid
  public Instant getCreated() {
    return created;
  }

  public void setCreated(Instant created) {
    this.created = created;
  }

  public Game started(Instant started) {
    this.started = started;
    return this;
  }

  /**
   * Get started
   *
   * @return started
   */
  @ApiModelProperty(readOnly = true, value = "")
  @Valid
  public Instant getStarted() {
    return started;
  }

  public void setStarted(Instant started) {
    this.started = started;
  }

  public Game finished(Instant finished) {
    this.finished = finished;
    return this;
  }

  /**
   * Get finished
   *
   * @return finished
   */
  @ApiModelProperty(readOnly = true, value = "")
  @Valid
  public Instant getFinished() {
    return finished;
  }

  public void setFinished(Instant finished) {
    this.finished = finished;
  }

  public Game elapsedSeconds(Long elapsedSeconds) {
    this.elapsedSeconds = elapsedSeconds;
    return this;
  }

  /**
   * Get elapsedSeconds
   *
   * @return elapsedSeconds
   */
  @ApiModelProperty(readOnly = true, value = "")
  public Long getElapsedSeconds() {
    return elapsedSeconds;
  }

  public void setElapsedSeconds(Long elapsedSeconds) {
    this.elapsedSeconds = elapsedSeconds;
  }

  public Game positions(List<Position> positions) {
    this.positions = positions;
    return this;
  }

  public Game addPositionsItem(Position positionsItem) {
    if (this.positions == null) {
      this.positions = new ArrayList<>();
    }
    this.positions.add(positionsItem);
    return this;
  }

  /**
   * positions on the board
   *
   * @return positions
   */
  @ApiModelProperty(readOnly = true, value = "positions on the board")
  @Valid
  public List<Position> getPositions() {
    return positions;
  }

  public void setPositions(List<Position> positions) {
    this.positions = positions;
  }

  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    Game game = (Game) o;
    return Objects.equals(this.id, game.id)
        && Objects.equals(this.rows, game.rows)
        && Objects.equals(this.columns, game.columns)
        && Objects.equals(this.mines, game.mines)
        && Objects.equals(this.status, game.status)
        && Objects.equals(this.created, game.created)
        && Objects.equals(this.started, game.started)
        && Objects.equals(this.finished, game.finished)
        && Objects.equals(this.elapsedSeconds, game.elapsedSeconds)
        && Objects.equals(this.positions, game.positions);
  }

  @Override
  public int hashCode() {
    return Objects.hash(
        id, rows, columns, mines, status, created, started, finished, elapsedSeconds, positions);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class Game {\n");

    sb.append("    id: ").append(toIndentedString(id)).append("\n");
    sb.append("    rows: ").append(toIndentedString(rows)).append("\n");
    sb.append("    columns: ").append(toIndentedString(columns)).append("\n");
    sb.append("    mines: ").append(toIndentedString(mines)).append("\n");
    sb.append("    status: ").append(toIndentedString(status)).append("\n");
    sb.append("    created: ").append(toIndentedString(created)).append("\n");
    sb.append("    started: ").append(toIndentedString(started)).append("\n");
    sb.append("    finished: ").append(toIndentedString(finished)).append("\n");
    sb.append("    elapsedSeconds: ").append(toIndentedString(elapsedSeconds)).append("\n");
    sb.append("    positions: ").append(toIndentedString(positions)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}
