package classicgames.minesweeper.app.adapter.rest.model;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonValue;
import io.swagger.annotations.ApiModelProperty;
import java.util.Objects;
import javax.validation.constraints.*;
import org.springframework.validation.annotation.Validated;

/** Position */
@Validated
@javax.annotation.Generated(
    value = "io.swagger.codegen.languages.SpringCodegen",
    date = "2021-06-03T02:48:29.920Z")
public class Position {
  @JsonProperty("row")
  private Integer row = null;

  @JsonProperty("column")
  private Integer column = null;

  /** Gets or Sets status */
  public enum StatusEnum {
    COVERED("COVERED"),

    UNCOVERED("UNCOVERED"),

    FLAG("FLAG"),

    QUESTION("QUESTION"),

    EXPLODED("EXPLODED");

    private String value;

    StatusEnum(String value) {
      this.value = value;
    }

    @Override
    @JsonValue
    public String toString() {
      return String.valueOf(value);
    }

    @JsonCreator
    public static StatusEnum fromValue(String text) {
      for (StatusEnum b : StatusEnum.values()) {
        if (String.valueOf(b.value).equals(text)) {
          return b;
        }
      }
      return null;
    }
  }

  @JsonProperty("status")
  private StatusEnum status = null;

  @JsonProperty("mine")
  private Boolean mine = null;

  public Position row(Integer row) {
    this.row = row;
    return this;
  }

  /**
   * Get row minimum: 0
   *
   * @return row
   */
  @ApiModelProperty(value = "")
  @Min(0)
  public Integer getRow() {
    return row;
  }

  public void setRow(Integer row) {
    this.row = row;
  }

  public Position column(Integer column) {
    this.column = column;
    return this;
  }

  /**
   * Get column minimum: 0
   *
   * @return column
   */
  @ApiModelProperty(value = "")
  @Min(0)
  public Integer getColumn() {
    return column;
  }

  public void setColumn(Integer column) {
    this.column = column;
  }

  public Position status(StatusEnum status) {
    this.status = status;
    return this;
  }

  /**
   * Get status
   *
   * @return status
   */
  @ApiModelProperty(readOnly = true, value = "")
  public StatusEnum getStatus() {
    return status;
  }

  public void setStatus(StatusEnum status) {
    this.status = status;
  }

  public Position mine(Boolean mine) {
    this.mine = mine;
    return this;
  }

  /**
   * Get mine
   *
   * @return mine
   */
  @ApiModelProperty(readOnly = true, value = "")
  public Boolean isMine() {
    return mine;
  }

  public void setMine(Boolean mine) {
    this.mine = mine;
  }

  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    Position position = (Position) o;
    return Objects.equals(this.row, position.row)
        && Objects.equals(this.column, position.column)
        && Objects.equals(this.status, position.status)
        && Objects.equals(this.mine, position.mine);
  }

  @Override
  public int hashCode() {
    return Objects.hash(row, column, status, mine);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class Position {\n");

    sb.append("    row: ").append(toIndentedString(row)).append("\n");
    sb.append("    column: ").append(toIndentedString(column)).append("\n");
    sb.append("    status: ").append(toIndentedString(status)).append("\n");
    sb.append("    mine: ").append(toIndentedString(mine)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}
