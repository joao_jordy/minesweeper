package classicgames.minesweeper.app.adapter.rest.api;

import classicgames.minesweeper.app.adapter.rest.model.User;
import classicgames.minesweeper.app.port.in.UserOperationPort;
import classicgames.minesweeper.app.port.in.UserQueryPort;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import io.swagger.annotations.Authorization;
import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@javax.annotation.Generated(
    value = "io.swagger.codegen.languages.SpringCodegen",
    date = "2021-05-31T01:09:08.889Z")
@Controller
@RequestMapping(value = "/")
public class UserApiController implements UserApi {

  @Autowired private UserOperationPort userOperationPort;

  @Autowired private UserQueryPort userQueryPort;

  // @formatter:off
  @ApiOperation(
      value = "Create user",
      nickname = "createUser",
      notes = "This can only be done by the logged in user.",
      tags = {
        "User",
      })
  @ApiResponses(value = {@ApiResponse(code = 200, message = "successful operation")})
  @RequestMapping(
      value = "/credential",
      produces = {"application/json"},
      consumes = {"application/json"},
      method = RequestMethod.POST) // @formatter:on
  public ResponseEntity<Void> createUser(
      @ApiParam(value = "Created user object", required = true) @Valid @RequestBody User body) {
    userOperationPort.createUser(body.getUsername(), body.getPassword());

    return ResponseEntity.ok().build();
  }

  // @formatter:off
  @ApiOperation(
      value = "Delete user",
      nickname = "deleteUser",
      notes = "This can only be done by the logged in user.",
      authorizations = {@Authorization(value = "api_key")},
      tags = {
        "User",
      })
  @ApiResponses(
      value = {
        @ApiResponse(code = 400, message = "Invalid username supplied"),
        @ApiResponse(code = 404, message = "User not found")
      })
  @RequestMapping(
      value = "/user",
      produces = {"application/json"},
      consumes = {"application/json"},
      method = RequestMethod.DELETE) // @formatter:on
  public ResponseEntity<Void> deleteUser() {
    userOperationPort.deleteLoggedUser();

    return ResponseEntity.ok().build();
  }

  // @formatter:off
  @ApiOperation(
      value = "Get user by user name",
      nickname = "getUserByName",
      notes = "",
      response = User.class,
      authorizations = {@Authorization(value = "api_key")},
      tags = {
        "User",
      })
  @ApiResponses(
      value = {
        @ApiResponse(code = 200, message = "successful operation", response = User.class),
        @ApiResponse(code = 400, message = "Invalid username supplied"),
        @ApiResponse(code = 404, message = "User not found")
      })
  @RequestMapping(
      value = "/user",
      produces = {"application/json"},
      consumes = {"application/json"},
      method = RequestMethod.GET) // @formatter:on
  public ResponseEntity<User> getUserByName() {
    final User user = userQueryPort.getLoggedUser();

    return ResponseEntity.ok(user);
  }

  // @formatter:off
  @ApiOperation(
      value = "Updated user",
      nickname = "updateUser",
      notes = "This can only be done by the logged in user.",
      authorizations = {@Authorization(value = "api_key")},
      tags = {
        "User",
      })
  @ApiResponses(
      value = {
        @ApiResponse(code = 400, message = "Invalid user supplied"),
        @ApiResponse(code = 404, message = "User not found")
      })
  @RequestMapping(
      value = "/user",
      produces = {"application/json"},
      consumes = {"application/json"},
      method = RequestMethod.PUT) // @formatter:on
  public ResponseEntity<Void> updateUser(
      @ApiParam(value = "Updated user object", required = true) @Valid @RequestBody User body) {
    userOperationPort.updateLoggedUser(body);

    return ResponseEntity.ok().build();
  }
}
