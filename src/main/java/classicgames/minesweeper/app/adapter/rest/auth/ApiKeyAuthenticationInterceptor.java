package classicgames.minesweeper.app.adapter.rest.auth;

import static java.lang.String.format;

import classicgames.minesweeper.app.adapter.tenant.InvalidTenantConfigurationException;
import classicgames.minesweeper.app.adapter.tenant.TenantAdapter;
import java.util.UUID;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.lang.Nullable;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.HandlerInterceptor;

@Slf4j
@Component
public class ApiKeyAuthenticationInterceptor implements HandlerInterceptor {

  private static final String X_API_KEY = "X-API-KEY";

  protected static final String[] AUTH_ALLOWLIST = //
      {"/index.html", //
          "/favicon.ico", //
          "/swagger-ui.html", //
          "/webjars/springfox-swagger-ui", //
          "/swagger-resources", //
          "/api-docs", //
          "/error", //
          "/csrf", //
          "/actuator", //
          "/credential", //
          "/login", //
          "/api"};

  @Autowired protected TenantAdapter tenant;

  @Override
  public void afterCompletion(HttpServletRequest request, HttpServletResponse response,
      Object handler, @Nullable Exception ex) throws Exception {
    tenant.clearCurrentTenant();
  }

  @Override
  public boolean preHandle(HttpServletRequest requestServlet, HttpServletResponse responseServlet,
      Object handler) throws Exception {

    return isAllowed(requestServlet) || isAuthenticationValid(requestServlet, responseServlet);
  }

  private boolean isAuthenticationValid(HttpServletRequest requestServlet,
      HttpServletResponse responseServlet) {
    final var requestApiKeyAsStr = requestServlet.getHeader(X_API_KEY);

    final var requestApiKey = parseUUID(requestApiKeyAsStr);

    if (tenant.configureTenant(requestApiKey).isPresent()) {
      return true;
    }

    return unauthorize(requestServlet, responseServlet, requestApiKey);
  }

  private UUID parseUUID(final String requestApiKeyAsStr) {
    UUID requestApiKey;
    try {
      requestApiKey = UUID.fromString(requestApiKeyAsStr);
    } catch (Exception e) {
      throw new InvalidTenantConfigurationException(requestApiKeyAsStr);
    }
    return requestApiKey;
  }

  private boolean isURIAllowed(HttpServletRequest requestServlet) {
    if (requestServlet.getRequestURI() == null) {
      return false;
    }

    for (final String uri : AUTH_ALLOWLIST) {
      if (requestServlet.getRequestURI().startsWith(uri)) {
        return true;
      }
    }

    return false;
  }

  private boolean isURIAsRoot(HttpServletRequest requestServlet) {
    return "/".equals(requestServlet.getRequestURI());
  }

  private boolean unauthorize(HttpServletRequest requestServlet,
      HttpServletResponse responseServlet, final UUID requestApiKey) {
    log.warn(format("Invalid api-key '%s'. Not Authenticated For URI: '%s'", requestApiKey,
        requestServlet.getRequestURI()));

    responseServlet.setStatus(HttpStatus.UNAUTHORIZED.value());

    return false;
  }

  protected boolean isAllowed(HttpServletRequest requestServlet) {
    log.debug("Endpoint: " + requestServlet.getRequestURI());

    if (isURIAsRoot(requestServlet) || isURIAllowed(requestServlet)) {
      log.debug("Authentication Allowlist. Ignoring : " + requestServlet.getRequestURI());
      return true;
    }

    return false;
  }
}
