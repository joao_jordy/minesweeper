/**
 * NOTE: This class is auto generated by the swagger code generator program (2.4.19).
 * https://github.com/swagger-api/swagger-codegen Do not edit the class manually.
 */
package classicgames.minesweeper.app.adapter.rest.api;

import classicgames.minesweeper.app.adapter.rest.model.Position;
import com.fasterxml.jackson.databind.ObjectMapper;
import io.swagger.annotations.*;
import java.util.Optional;
import java.util.UUID;
import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import javax.validation.constraints.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@javax.annotation.Generated(
    value = "io.swagger.codegen.languages.SpringCodegen",
    date = "2021-06-03T02:48:29.920Z")
@Validated
@Api(value = "Move", description = "the Move API")
@RequestMapping(value = "/joao_jordy/Minesweeper/1.0.0")
public interface MoveApi {

  Logger log = LoggerFactory.getLogger(MoveApi.class);

  default Optional<ObjectMapper> getObjectMapper() {
    return Optional.empty();
  }

  default Optional<HttpServletRequest> getRequest() {
    return Optional.empty();
  }

  default Optional<String> getAcceptHeader() {
    return getRequest().map(r -> r.getHeader("Accept"));
  }

  @ApiOperation(
      value = "Unset question mark to a position",
      nickname = "deleteQuestionMarkPosition",
      notes = "",
      authorizations = {@Authorization(value = "api_key")},
      tags = {
        "Move",
      })
  @ApiResponses(
      value = {
        @ApiResponse(code = 400, message = "Invalid ID supplied"),
        @ApiResponse(code = 404, message = "Game not found"),
        @ApiResponse(code = 405, message = "Validation exception")
      })
  @RequestMapping(
      value = "/game/{game_id}/question_mark",
      produces = {"application/json"},
      consumes = {"application/json"},
      method = RequestMethod.DELETE)
  default ResponseEntity<Void> deleteQuestionMarkPosition(
      @ApiParam(value = "ID of game to return", required = true) @PathVariable("game_id")
          UUID gameId,
      @ApiParam(value = "Game object that needs to be added to the user", required = true)
          @Valid
          @RequestBody
          Position body) {
    if (getObjectMapper().isPresent() && getAcceptHeader().isPresent()) {
    } else {
      log.warn(
          "ObjectMapper or HttpServletRequest not configured in default MoveApi interface so no example is generated");
    }
    return new ResponseEntity<>(HttpStatus.NOT_IMPLEMENTED);
  }

  @ApiOperation(
      value = "Unset red flag to a position",
      nickname = "deleteRedFlagPosition",
      notes = "",
      authorizations = {@Authorization(value = "api_key")},
      tags = {
        "Move",
      })
  @ApiResponses(
      value = {
        @ApiResponse(code = 400, message = "Invalid ID supplied"),
        @ApiResponse(code = 404, message = "Game not found"),
        @ApiResponse(code = 405, message = "Validation exception")
      })
  @RequestMapping(
      value = "/game/{game_id}/red_flag",
      produces = {"application/json"},
      consumes = {"application/json"},
      method = RequestMethod.DELETE)
  default ResponseEntity<Void> deleteRedFlagPosition(
      @ApiParam(value = "ID of game to return", required = true) @PathVariable("game_id")
          UUID gameId,
      @ApiParam(value = "Game object that needs to be added to the user", required = true)
          @Valid
          @RequestBody
          Position body) {
    if (getObjectMapper().isPresent() && getAcceptHeader().isPresent()) {
    } else {
      log.warn(
          "ObjectMapper or HttpServletRequest not configured in default MoveApi interface so no example is generated");
    }
    return new ResponseEntity<>(HttpStatus.NOT_IMPLEMENTED);
  }

  @ApiOperation(
      value = "Set question mark to a position",
      nickname = "questionMarkPosition",
      notes = "",
      authorizations = {@Authorization(value = "api_key")},
      tags = {
        "Move",
      })
  @ApiResponses(
      value = {
        @ApiResponse(code = 400, message = "Invalid ID supplied"),
        @ApiResponse(code = 404, message = "Game not found"),
        @ApiResponse(code = 405, message = "Validation exception")
      })
  @RequestMapping(
      value = "/game/{game_id}/question_mark",
      produces = {"application/json"},
      consumes = {"application/json"},
      method = RequestMethod.PUT)
  default ResponseEntity<Void> questionMarkPosition(
      @ApiParam(value = "ID of game to return", required = true) @PathVariable("game_id")
          UUID gameId,
      @ApiParam(value = "Game object that needs to be added to the user", required = true)
          @Valid
          @RequestBody
          Position body) {
    if (getObjectMapper().isPresent() && getAcceptHeader().isPresent()) {
    } else {
      log.warn(
          "ObjectMapper or HttpServletRequest not configured in default MoveApi interface so no example is generated");
    }
    return new ResponseEntity<>(HttpStatus.NOT_IMPLEMENTED);
  }

  @ApiOperation(
      value = "Set red flag to a position",
      nickname = "redFlagPosition",
      notes = "",
      authorizations = {@Authorization(value = "api_key")},
      tags = {
        "Move",
      })
  @ApiResponses(
      value = {
        @ApiResponse(code = 400, message = "Invalid ID supplied"),
        @ApiResponse(code = 404, message = "Game not found"),
        @ApiResponse(code = 405, message = "Validation exception")
      })
  @RequestMapping(
      value = "/game/{game_id}/red_flag",
      produces = {"application/json"},
      consumes = {"application/json"},
      method = RequestMethod.PUT)
  default ResponseEntity<Void> redFlagPosition(
      @ApiParam(value = "ID of game to return", required = true) @PathVariable("game_id")
          UUID gameId,
      @ApiParam(value = "Game object that needs to be added to the user", required = true)
          @Valid
          @RequestBody
          Position body) {
    if (getObjectMapper().isPresent() && getAcceptHeader().isPresent()) {
    } else {
      log.warn(
          "ObjectMapper or HttpServletRequest not configured in default MoveApi interface so no example is generated");
    }
    return new ResponseEntity<>(HttpStatus.NOT_IMPLEMENTED);
  }

  @ApiOperation(
      value = "Turn a position (uncover)",
      nickname = "turnPosition",
      notes = "",
      authorizations = {@Authorization(value = "api_key")},
      tags = {
        "Move",
      })
  @ApiResponses(
      value = {
        @ApiResponse(code = 400, message = "Invalid ID supplied"),
        @ApiResponse(code = 404, message = "Game not found"),
        @ApiResponse(code = 405, message = "Validation exception")
      })
  @RequestMapping(
      value = "/game/{game_id}/turn",
      produces = {"application/json"},
      consumes = {"application/json"},
      method = RequestMethod.PUT)
  default ResponseEntity<Void> turnPosition(
      @ApiParam(value = "ID of game to return", required = true) @PathVariable("game_id")
          UUID gameId,
      @ApiParam(value = "Game object that needs to be added to the user", required = true)
          @Valid
          @RequestBody
          Position body) {
    if (getObjectMapper().isPresent() && getAcceptHeader().isPresent()) {
    } else {
      log.warn(
          "ObjectMapper or HttpServletRequest not configured in default MoveApi interface so no example is generated");
    }
    return new ResponseEntity<>(HttpStatus.NOT_IMPLEMENTED);
  }
}
