package classicgames.minesweeper.app.adapter.rest.api;

import classicgames.minesweeper.app.adapter.rest.model.Position;
import classicgames.minesweeper.app.port.in.MoveOperationPort;
import classicgames.minesweeper.app.port.in.MoveTurnPositionPort;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import io.swagger.annotations.Authorization;
import java.util.UUID;
import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@javax.annotation.Generated(
    value = "io.swagger.codegen.languages.SpringCodegen",
    date = "2021-06-01T02:52:45.874Z")
@Controller
@RequestMapping(value = "/")
public class MoveApiController implements MoveApi {

  @Autowired MoveOperationPort moveOperationPort;

  @Autowired MoveTurnPositionPort moveTurnPositionPort;

  // @formatter:off
  @ApiOperation(
      value = "Update an existing game",
      nickname = "deleteQuestionMarkPosition",
      notes = "",
      authorizations = {@Authorization(value = "api_key")},
      tags = {
        "Move",
      })
  @ApiResponses(
      value = {
        @ApiResponse(code = 400, message = "Invalid ID supplied"),
        @ApiResponse(code = 404, message = "Game not found"),
        @ApiResponse(code = 405, message = "Validation exception")
      })
  @RequestMapping(
      value = "/game/{game_id}/question_mark",
      produces = {"application/json"},
      consumes = {"application/json"},
      method = RequestMethod.DELETE) // @formatter:on
  public ResponseEntity<Void> deleteQuestionMarkPosition(
      @ApiParam(value = "ID of game to return", required = true) @PathVariable("game_id")
          UUID gameId,
      @ApiParam(value = "Game object that needs to be added to the user", required = true)
          @Valid
          @RequestBody
          Position body) {
    moveOperationPort.unsetQuestionMark(gameId, body.getRow(), body.getColumn());

    return ResponseEntity.ok().build();
  }

  // @formatter:off
  @ApiOperation(
      value = "Update an existing game",
      nickname = "deleteRedFlagPosition",
      notes = "",
      authorizations = {@Authorization(value = "api_key")},
      tags = {
        "Move",
      })
  @ApiResponses(
      value = {
        @ApiResponse(code = 400, message = "Invalid ID supplied"),
        @ApiResponse(code = 404, message = "Game not found"),
        @ApiResponse(code = 405, message = "Validation exception")
      })
  @RequestMapping(
      value = "/game/{game_id}/red_flag",
      produces = {"application/json"},
      consumes = {"application/json"},
      method = RequestMethod.DELETE) // @formatter:on
  public ResponseEntity<Void> deleteRedFlagPosition(
      @ApiParam(value = "ID of game to return", required = true) @PathVariable("game_id")
          UUID gameId,
      @ApiParam(value = "Game object that needs to be added to the user", required = true)
          @Valid
          @RequestBody
          Position body) {
    moveOperationPort.unsetRedFlag(gameId, body.getRow(), body.getColumn());

    return ResponseEntity.ok().build();
  }

  // @formatter:off
  @ApiOperation(
      value = "Update an existing game",
      nickname = "questionMarkPosition",
      notes = "",
      authorizations = {@Authorization(value = "api_key")},
      tags = {
        "Move",
      })
  @ApiResponses(
      value = {
        @ApiResponse(code = 400, message = "Invalid ID supplied"),
        @ApiResponse(code = 404, message = "Game not found"),
        @ApiResponse(code = 405, message = "Validation exception")
      })
  @RequestMapping(
      value = "/game/{game_id}/question_mark",
      produces = {"application/json"},
      consumes = {"application/json"},
      method = RequestMethod.PUT) // @formatter:on
  public ResponseEntity<Void> questionMarkPosition(
      @ApiParam(value = "ID of game to return", required = true) @PathVariable("game_id")
          UUID gameId,
      @ApiParam(value = "Game object that needs to be added to the user", required = true)
          @Valid
          @RequestBody
          Position body) {
    moveOperationPort.setQuestionMark(gameId, body.getRow(), body.getColumn());

    return ResponseEntity.ok().build();
  }

  // @formatter:off
  @ApiOperation(
      value = "Update an existing game",
      nickname = "redFlagPosition",
      notes = "",
      authorizations = {@Authorization(value = "api_key")},
      tags = {
        "Move",
      })
  @ApiResponses(
      value = {
        @ApiResponse(code = 400, message = "Invalid ID supplied"),
        @ApiResponse(code = 404, message = "Game not found"),
        @ApiResponse(code = 405, message = "Validation exception")
      })
  @RequestMapping(
      value = "/game/{game_id}/red_flag",
      produces = {"application/json"},
      consumes = {"application/json"},
      method = RequestMethod.PUT) // @formatter:on
  public ResponseEntity<Void> redFlagPosition(
      @ApiParam(value = "ID of game to return", required = true) @PathVariable("game_id")
          UUID gameId,
      @ApiParam(value = "Game object that needs to be added to the user", required = true)
          @Valid
          @RequestBody
          Position body) {
    moveOperationPort.setRedFlag(gameId, body.getRow(), body.getColumn());

    return ResponseEntity.ok().build();
  }

  // @formatter:off
  @ApiOperation(
      value = "Update an existing game",
      nickname = "turnPosition",
      notes = "",
      authorizations = {@Authorization(value = "api_key")},
      tags = {
        "Move",
      })
  @ApiResponses(
      value = {
        @ApiResponse(code = 400, message = "Invalid ID supplied"),
        @ApiResponse(code = 404, message = "Game not found"),
        @ApiResponse(code = 405, message = "Validation exception")
      })
  @RequestMapping(
      value = "/game/{game_id}/turn",
      produces = {"application/json"},
      consumes = {"application/json"},
      method = RequestMethod.PUT) // @formatter:on
  public ResponseEntity<Void> turnPosition(
      @ApiParam(value = "ID of game to return", required = true) @PathVariable("game_id")
          UUID gameId,
      @ApiParam(value = "Game object that needs to be added to the user", required = true)
          @Valid
          @RequestBody
          Position body) {
    moveTurnPositionPort.turnPosition(gameId, body.getRow(), body.getColumn());

    return ResponseEntity.ok().build();
  }
}
