package classicgames.minesweeper.app.adapter.builder;

import static java.time.Instant.now;
import static java.util.UUID.randomUUID;

import classicgames.minesweeper.app.persistency.domain.GameEntity;
import classicgames.minesweeper.app.persistency.domain.PositionEntity;
import classicgames.minesweeper.app.persistency.domain.UserEntity;
import java.util.HashSet;
import java.util.Optional;
import java.util.Random;
import java.util.Set;
import org.springframework.stereotype.Component;

@Component
public class NewGameBuilder {

  public GameEntity build(int rows, int columns, int mines, long userId) {
    final var positions = buildPositions(rows, columns, mines);

    return new GameEntity() //
        .externalId(randomUUID()) //
        .status(GameEntity.Status.CREATED) //
        .created(now()) //
        .rows(rows) //
        .columns(columns) //
        .mines(mines) //
        .user(new UserEntity().id(userId)) //
        .position(positions);
  }

  private PositionEntity buildPositionEntity(int row, int column) {
    return new PositionEntity() //
        .rowNumber(row) //
        .columnNumber(column) //
        .status(PositionEntity.Status.COVERED);
  }

  private Set<PositionEntity> buildPositions(int rows, int columns, int mines) {
    validateInputs(rows, columns, mines);

    final var positions = buildPositionSet(rows, columns);

    setMinesRecursively(rows, columns, mines, positions);

    return positions;
  }

  private HashSet<PositionEntity> buildPositionSet(int rows, int columns) {
    final var positions = new HashSet<PositionEntity>();

    for (int i = 0; i < rows; i++) {
      for (int j = 0; j < columns; j++) {
        positions.add(buildPositionEntity(i, j));
      }
    }

    return positions;
  }

  private long countMines(Set<PositionEntity> positions) {
    return positions.stream().filter(p -> p.hasMine()).count();
  }

  private Optional<PositionEntity> findPosition(
      Set<PositionEntity> positions, int row, int column) {
    return positions.stream()
        .filter(p -> p.rowNumber() == row && p.columnNumber() == column)
        .findFirst();
  }

  private int getRandom(int max) {
    return new Random().ints(0, max).findFirst().getAsInt();
  }

  private void setMineIfPositionIsAvailable(int rows, int columns, Set<PositionEntity> positions) {
    final var position = findPosition(positions, getRandom(rows), getRandom(columns));

    if (position.isPresent() && !position.get().hasMine()) {
      position.get().hasMine(true);
    }
  }

  // TODO optimize counting, currently > O(N*mines)
  private void setMinesRecursively(
      int rows, int columns, int mines, Set<PositionEntity> positions) {

    if (countMines(positions) >= mines) {
      return; // end of recursion
    }

    setMineIfPositionIsAvailable(rows, columns, positions);

    setMinesRecursively(rows, columns, mines, positions); // recursion
  }

  private void validateInputs(int rows, int columns, int mines) {
    if (mines > rows * columns) {
      throw new IllegalArgumentException(
          "The number of mines is bigger than the number of positions!");
    }
  }
}
