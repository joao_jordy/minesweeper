package classicgames.minesweeper.app.usecase.game;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.verify;

import classicgames.minesweeper.app.adapter.builder.NewGameBuilder;
import classicgames.minesweeper.app.adapter.tenant.Tenant;
import classicgames.minesweeper.app.adapter.tenant.TenantAdapter;
import classicgames.minesweeper.app.persistency.domain.GameEntity;
import classicgames.minesweeper.app.persistency.repository.GameRepository;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Spy;
import org.mockito.junit.jupiter.MockitoExtension;

@ExtendWith(MockitoExtension.class)
class GameOperationUseCaseTest {

  @Spy NewGameBuilder builder;

  @Mock GameRepository repository;

  @InjectMocks GameOperationUseCase useCase;

  @Mock TenantAdapter tenant;

  @Test
  void when_creatingGameUseCase_then_success() {
    int rows = 10;
    int columns = 10;
    int mines = 10;

    doReturn(new Tenant().userId(1L)).when(tenant).getCurrentTenant();

    useCase.create(rows, columns, mines);

    verify(builder).build(10, 10, 10, 1L);
    verify(repository).save(any(GameEntity.class));
  }
}
