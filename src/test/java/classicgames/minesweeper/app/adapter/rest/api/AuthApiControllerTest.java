package classicgames.minesweeper.app.adapter.rest.api;

import static classicgames.minesweeper.app.adapter.rest.api.helper.AuthenticationTestHelper.login;
import static classicgames.minesweeper.app.adapter.rest.api.helper.AuthenticationTestHelper.logout;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.web.servlet.MockMvc;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@AutoConfigureMockMvc
class AuthApiControllerTest {

  @Autowired private MockMvc mockmvc;

  @Test
  void when_login_then_success() throws Exception {
    login(mockmvc);
  }

  @Test
  void when_logout_then_success() throws Exception {
    final var apiKey = login(mockmvc);

    logout(mockmvc, apiKey);
  }
}
