package classicgames.minesweeper.app.adapter.rest.api.helper;

import static com.fasterxml.jackson.databind.SerializationFeature.WRAP_ROOT_VALUE;

import classicgames.minesweeper.app.adapter.rest.model.Game;
import classicgames.minesweeper.app.adapter.rest.model.Login;
import classicgames.minesweeper.app.adapter.rest.model.Position;
import classicgames.minesweeper.app.adapter.rest.model.User;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.commons.lang3.RandomStringUtils;

public class ContentTestHelper {

  public static String generateUsername() {
    return RandomStringUtils.randomAlphabetic(10);
  }

  public static String buildGameContent() throws JsonProcessingException {
    return new ObjectMapper() //
        .configure(WRAP_ROOT_VALUE, false) //
        .writer()
        .withDefaultPrettyPrinter() //
        .writeValueAsString(new Game().rows(2).columns(2).mines(1));
  }

  public static String buildLoginContent(String username) throws JsonProcessingException {
    return new ObjectMapper() //
        .configure(WRAP_ROOT_VALUE, false) //
        .writer()
        .withDefaultPrettyPrinter() //
        .writeValueAsString(new Login().username(username).password("password"));
  }

  public static String buildPositionContent() throws JsonProcessingException {
    return new ObjectMapper() //
        .configure(WRAP_ROOT_VALUE, false) //
        .writer()
        .withDefaultPrettyPrinter() //
        .writeValueAsString(new Position().row(1).column(1));
  }

  public static String buildUserContent() throws JsonProcessingException {
    return new ObjectMapper() //
        .configure(WRAP_ROOT_VALUE, false) //
        .writer()
        .withDefaultPrettyPrinter() //
        .writeValueAsString(new User().username("username").password("password"));
  }

  public static String buildUserContent(String username) throws JsonProcessingException {
    return new ObjectMapper() //
        .configure(WRAP_ROOT_VALUE, false) //
        .writer()
        .withDefaultPrettyPrinter() //
        .writeValueAsString(new User().username(username).password("password"));
  }
}
