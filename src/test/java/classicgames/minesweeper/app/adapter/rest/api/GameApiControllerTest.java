package classicgames.minesweeper.app.adapter.rest.api;

import static classicgames.minesweeper.app.adapter.rest.api.helper.AuthenticationTestHelper.login;
import static classicgames.minesweeper.app.adapter.rest.api.helper.ContentTestHelper.buildGameContent;
import static classicgames.minesweeper.app.adapter.rest.api.helper.GameTestHelper.createGame;
import static classicgames.minesweeper.app.adapter.rest.api.helper.GameTestHelper.startGame;
import static org.springframework.http.MediaType.APPLICATION_JSON;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.web.servlet.MockMvc;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@AutoConfigureMockMvc
@ExtendWith(MockitoExtension.class)
class GameApiControllerTest {

  @Autowired private MockMvc mockmvc;

  @Test
  void when_createGame_then_success() throws Exception {
    final var apiKey = login(mockmvc);

    mockmvc
        .perform(
            post("/game") //
                .contentType(APPLICATION_JSON) //
                .content(buildGameContent()) //
                .header("X-API-KEY", apiKey)) //
        .andExpect(status().is2xxSuccessful())
        .andExpect(
            content()
                .json(
                    "{\"rows\":2,\"columns\":2,\"mines\":1,\"status\":\"CREATED\",\"started\":null,\"finished\":null,\"elapsed_seconds\":0}"));
  }

  @Test
  void when_deleteGame_then_success() throws Exception {
    final var apiKey = login(mockmvc);

    final var gameUuid = createGame(mockmvc, apiKey);

    mockmvc
        .perform(
            delete("/game/{game_id}", gameUuid) //
                .contentType(APPLICATION_JSON) //
                .header("X-API-KEY", apiKey)) //
        .andExpect(status().is2xxSuccessful());
  }

  @Test
  void when_findAllGames_then_empty() throws Exception {
    final var apiKey = login(mockmvc);

    mockmvc
        .perform(
            get("/game") //
                .contentType(APPLICATION_JSON) //
                .header("X-API-KEY", apiKey)) //
        .andExpect(status().is2xxSuccessful()) //
        .andExpect(content().json("[]"));
  }

  @Test
  void when_findAllGames_then_NoResults() throws Exception {
    final var apiKey = login(mockmvc);

    mockmvc
        .perform(
            get("/game") //
                .contentType(APPLICATION_JSON) //
                .header("X-API-KEY", apiKey)) //
        .andExpect(status().is2xxSuccessful());
  }

  @Test
  void when_findByGameId_then_success() throws Exception {
    final var apiKey = login(mockmvc);

    final var gameUuid = createGame(mockmvc, apiKey);

    mockmvc
        .perform(
            get("/game/{game_id}", gameUuid) //
                .contentType(APPLICATION_JSON) //
                .header("X-API-KEY", apiKey) //
                .content(buildGameContent())) //
        .andExpect(status().is2xxSuccessful())
        .andExpect(
            content()
                .json(
                    "{\"rows\":2,\"columns\":2,\"mines\":1,\"status\":CREATED,\"started\":null,\"finished\":null,\"elapsed_seconds\":0}"));
  }

  @Test
  void when_pauseGame_then_success() throws Exception {
    final var apiKey = login(mockmvc);

    final var gameUuid = createGame(mockmvc, apiKey);

    startGame(mockmvc, apiKey, gameUuid);

    mockmvc
        .perform(
            put("/game/{game_id}/pause", gameUuid) //
                .contentType(APPLICATION_JSON) //
                .header("X-API-KEY", apiKey)) //
        .andExpect(status().is2xxSuccessful());
  }

  @Test
  void when_resumeGame_then_success() throws Exception {
    final var apiKey = login(mockmvc);

    final var gameUuid = createGame(mockmvc, apiKey);

    startGame(mockmvc, apiKey, gameUuid);

    mockmvc
        .perform(
            put("/game/{game_id}/pause", gameUuid) //
                .contentType(APPLICATION_JSON) //
                .header("X-API-KEY", apiKey)) //
        .andExpect(status().is2xxSuccessful());

    mockmvc
        .perform(
            put("/game/{game_id}/resume", gameUuid) //
                .contentType(APPLICATION_JSON) //
                .header("X-API-KEY", apiKey)) //
        .andExpect(status().is2xxSuccessful());
  }

  @Test
  void when_startGame_then_success() throws Exception {
    final var apiKey = login(mockmvc);

    final var gameUuid = createGame(mockmvc, apiKey);

    startGame(mockmvc, apiKey, gameUuid);
    ;
  }
}
