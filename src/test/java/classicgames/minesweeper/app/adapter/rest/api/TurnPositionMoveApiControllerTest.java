package classicgames.minesweeper.app.adapter.rest.api;

import static classicgames.minesweeper.app.adapter.rest.api.helper.AuthenticationTestHelper.login;
import static classicgames.minesweeper.app.adapter.rest.api.helper.ContentTestHelper.buildPositionContent;
import static classicgames.minesweeper.app.adapter.rest.api.helper.GameTestHelper.createGame;
import static classicgames.minesweeper.app.adapter.rest.api.helper.GameTestHelper.startGame;
import static classicgames.minesweeper.app.adapter.rest.api.helper.MoveTestHelper.getAndExpect;
import static org.springframework.http.MediaType.APPLICATION_JSON;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import classicgames.minesweeper.app.adapter.rest.api.helper.JsonTestHelper;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.web.servlet.MockMvc;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@AutoConfigureMockMvc
class TurnPositionMoveApiControllerTest {

  @Autowired private MockMvc mockmvc;

  @Autowired JsonTestHelper json;

  @Disabled("Due to randomic mines distribution, it's hard to assert the same final state.")
  @Test
  void when_uncover_then_success() throws Exception {
    final var apiKey = login(mockmvc);

    final var gameUuid = createGame(mockmvc, apiKey);

    startGame(mockmvc, apiKey, gameUuid);

    getAndExpect(
        mockmvc, apiKey, gameUuid, json.gameWith2RowsAnd2ColumnsAnd1MineAndRunningAndAllCovered());

    mockmvc
        .perform(
            put("/game/{game_id}/turn", gameUuid) //
                .contentType(APPLICATION_JSON) //
                .header("X-API-KEY", apiKey) //
                .content(buildPositionContent()))
        .andExpect(status().is2xxSuccessful());

    getAndExpect(
        mockmvc,
        apiKey,
        gameUuid,
        json.gameWith2RowsAnd2ColumnsAnd1MineAndRunningAndOneUncovered());
  }
}
