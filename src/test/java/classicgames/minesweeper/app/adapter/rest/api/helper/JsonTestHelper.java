package classicgames.minesweeper.app.adapter.rest.api.helper;

import static java.nio.file.Files.readAllBytes;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.Resource;
import org.springframework.stereotype.Component;

@Component
public class JsonTestHelper {

  @Value("classpath:json/gameWith2RowsAnd2ColumnsAnd1MineAndRunningAndOneRedFlagAndAllCovered.json")
  private Resource gameWith2RowsAnd2ColumnsAnd1MineAndRunningAndOneRedFlagAndAllCovered;

  @Value("classpath:json/gameWith2RowsAnd2ColumnsAnd1MineAndRunningAndAllCovered.json")
  Resource gameWith2RowsAnd2ColumnsAnd1MineAndRunningAndAllCovered;

  @Value(
      "classpath:json/gameWith2RowsAnd2ColumnsAnd1MineAndRunningAndOneQuestionMarkAndAllCovered.json")
  Resource gameWith2RowsAnd2ColumnsAnd1MineAndRunningAndOneQuestionMarkAndAllCovered;

  @Value("classpath:json/gameWith2RowsAnd2ColumnsAnd1MineAndRunningAndOneUncovered.json")
  Resource gameWith2RowsAnd2ColumnsAnd1MineAndRunningAndOneUncovered;

  public String gameWith2RowsAnd2ColumnsAnd1MineAndRunningAndAllCovered() {
    return readFileAsString(gameWith2RowsAnd2ColumnsAnd1MineAndRunningAndAllCovered);
  }

  public String gameWith2RowsAnd2ColumnsAnd1MineAndRunningAndOneQuestionMarkAndAllCovered() {
    return readFileAsString(
        gameWith2RowsAnd2ColumnsAnd1MineAndRunningAndOneQuestionMarkAndAllCovered);
  }

  public String gameWith2RowsAnd2ColumnsAnd1MineAndRunningAndOneRedFlagAndAllCovered() {
    return readFileAsString(gameWith2RowsAnd2ColumnsAnd1MineAndRunningAndOneRedFlagAndAllCovered);
  }

  public String gameWith2RowsAnd2ColumnsAnd1MineAndRunningAndOneUncovered() {
    return readFileAsString(gameWith2RowsAnd2ColumnsAnd1MineAndRunningAndOneUncovered);
  }

  private String readFileAsString(Resource game2x21mRunningAllCovered2) {
    try {
      return new String(readAllBytes(game2x21mRunningAllCovered2.getFile().toPath()));
    } catch (Exception e) {
      throw new RuntimeException(e);
    }
  }
}
