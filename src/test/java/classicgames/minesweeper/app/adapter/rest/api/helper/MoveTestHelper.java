package classicgames.minesweeper.app.adapter.rest.api.helper;

import static org.springframework.http.MediaType.APPLICATION_JSON;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.springframework.test.web.servlet.MockMvc;

public class MoveTestHelper {

  public static void getAndExpect(MockMvc mockmvc, String apiKey, String gameUuid, String json)
      throws Exception {
    mockmvc
        .perform(
            get("/game/{game_id}", gameUuid) //
                .contentType(APPLICATION_JSON) //
                .header("X-API-KEY", apiKey)) //
        .andExpect(status().is2xxSuccessful()) //
        .andExpect(content().json(json));
  }
}
