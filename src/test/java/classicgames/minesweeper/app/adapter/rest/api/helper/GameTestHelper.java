package classicgames.minesweeper.app.adapter.rest.api.helper;

import static classicgames.minesweeper.app.adapter.rest.api.helper.ContentTestHelper.buildGameContent;
import static org.springframework.http.MediaType.APPLICATION_JSON;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import com.jayway.jsonpath.JsonPath;
import org.springframework.test.web.servlet.MockMvc;

public class GameTestHelper {

  public static String createGame(MockMvc mockmvc, final String apiKey) throws Exception {

    final var content =
        mockmvc
            .perform(
                post("/game") //
                    .contentType(APPLICATION_JSON) //
                    .content(buildGameContent()) //
                    .header("X-API-KEY", apiKey)) //
            .andExpect(status().is2xxSuccessful()) //
            .andReturn()
            .getResponse()
            .getContentAsString();

    return JsonPath.read(content, "$.id");
  }

  public static void startGame(MockMvc mockmvc, String apiKey, String gameUuid) throws Exception {
    mockmvc
        .perform(
            put("/game/{game_id}/start", gameUuid) //
                .contentType(APPLICATION_JSON) //
                .header("X-API-KEY", apiKey)) //
        .andExpect(status().is2xxSuccessful());
  }
}
