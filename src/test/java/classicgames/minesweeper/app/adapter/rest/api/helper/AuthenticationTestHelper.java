package classicgames.minesweeper.app.adapter.rest.api.helper;

import static classicgames.minesweeper.app.adapter.rest.api.helper.ContentTestHelper.buildLoginContent;
import static classicgames.minesweeper.app.adapter.rest.api.helper.ContentTestHelper.buildUserContent;
import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;
import static org.springframework.http.MediaType.APPLICATION_JSON;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import com.jayway.jsonpath.JsonPath;
import java.util.UUID;
import org.apache.commons.lang3.RandomStringUtils;
import org.springframework.test.web.servlet.MockMvc;

public class AuthenticationTestHelper {

  public static String authenticate(MockMvc mockmvc, final String username) throws Exception {
    final var content =
        mockmvc
            .perform(
                put("/login") //
                    .contentType(APPLICATION_JSON) //
                    .content(buildLoginContent(username))) //
            .andExpect(status().is2xxSuccessful()) //
            .andReturn()
            .getResponse()
            .getContentAsString();

    final String apiKey = JsonPath.read(content, "$");

    return assertDoesNotThrow(() -> UUID.fromString(apiKey)).toString();
  }

  public static String createCredential(MockMvc mockmvc) throws Exception {
    final var username = generateUsername();

    mockmvc
        .perform(
            post("/credential") //
                .contentType(APPLICATION_JSON) //
                .content(buildUserContent(username))) //
        .andExpect(status().is2xxSuccessful()) //
        .andReturn()
        .getResponse()
        .getContentAsString();

    return username;
  }

  public static String generateUsername() {
    return RandomStringUtils.randomAlphabetic(10);
  }

  public static String login(MockMvc mockmvc) throws Exception {
    final var username = createCredential(mockmvc);

    return authenticate(mockmvc, username);
  }

  public static void logout(MockMvc mockmvc, String apiKey) throws Exception {
    mockmvc
        .perform(
            put("/logout") //
                .contentType(APPLICATION_JSON) //
                .header("X-API-KEY", apiKey)) //
        .andExpect(status().is2xxSuccessful()) //
        .andReturn()
        .getResponse()
        .getContentAsString();
  }
}
