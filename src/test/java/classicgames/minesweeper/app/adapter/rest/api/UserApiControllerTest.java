package classicgames.minesweeper.app.adapter.rest.api;

import static classicgames.minesweeper.app.adapter.rest.api.helper.ContentTestHelper.buildUserContent;
import static org.springframework.http.MediaType.APPLICATION_JSON;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.web.servlet.MockMvc;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@AutoConfigureMockMvc
class UserApiControllerTest {

  @Autowired private MockMvc mockmvc;

  @Test
  void when_createUser_then_success() throws Exception {
    mockmvc
        .perform(
            post("/credential") //
                .contentType(APPLICATION_JSON) //
                .content(buildUserContent())) //
        .andExpect(status().is2xxSuccessful()) //
        .andReturn()
        .getResponse()
        .getContentAsString();
  }
}
