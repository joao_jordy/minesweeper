package classicgames.minesweeper.app.adapter.rest.auth;

import static java.util.Optional.of;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.when;

import classicgames.minesweeper.app.adapter.tenant.Tenant;
import classicgames.minesweeper.app.adapter.tenant.TenantAdapter;
import java.util.UUID;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

@ExtendWith(MockitoExtension.class)
class ApiKeyAuthenticationInterceptorTest {

  @Mock Object handler;

  @InjectMocks ApiKeyAuthenticationInterceptor interceptor;

  @Mock HttpServletRequest requestServlet;

  @Mock HttpServletResponse responseServlet;

  @Mock TenantAdapter tenant;

  @Test
  void whenApiUriAndValidApiKeyThenReturnTrue() throws Exception {
    doReturn("/v1").when(requestServlet).getRequestURI();
    when(requestServlet.getHeader("X-API-KEY")).thenReturn(UUID.randomUUID().toString());
    doReturn(of(new Tenant())).when(tenant).configureTenant(any(UUID.class));

    assertTrue(interceptor.preHandle(requestServlet, responseServlet, handler));
  }

  @Test
  void whenPreHandleWithConfigUriThenReturnTrue() throws Exception {
    doReturn("/swagger-ui.html").when(requestServlet).getRequestURI();

    assertTrue(interceptor.preHandle(requestServlet, responseServlet, handler));
  }

  @Test
  void whenPreHandleWithInvalidUriAndInvalidApiKeyThenReturnFalse() throws Exception {
    doReturn("/invalid").when(requestServlet).getRequestURI();

    doReturn(UUID.randomUUID().toString()).when(requestServlet).getHeader("X-API-KEY");

    assertFalse(interceptor.preHandle(requestServlet, responseServlet, handler));
  }

  @Test
  void whenPreHandleWithRootUriAndInvalidApiKeyThenReturnTrue() throws Exception {
    doReturn("/").when(requestServlet).getRequestURI();
    assertTrue(interceptor.preHandle(requestServlet, responseServlet, handler));
  }

  @ParameterizedTest
  @ValueSource(
      strings = {
        "/", //
        "/swagger-ui.html", //
        "/webjars/springfox-swagger-ui", //
        "/swagger-resources", //
        "/error", //
        "/csrf", //
        "/actuator", //
        "/credential"
      })
  void whenRequestUriIsAllowedThenTrue(String uri) {
    doReturn(uri).when(requestServlet).getRequestURI();

    assertTrue(interceptor.isAllowed(requestServlet));
  }

  @Test
  void whenRequestUriNullThenReturnFalse() {
    doReturn(null).when(requestServlet).getRequestURI();

    assertFalse(interceptor.isAllowed(requestServlet));
  }
}
