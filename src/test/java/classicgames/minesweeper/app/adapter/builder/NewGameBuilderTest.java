package classicgames.minesweeper.app.adapter.builder;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

import org.junit.jupiter.api.Test;

class NewGameBuilderTest {

  private void executeAndAssert(final int rows, final int columns, final int mines) {
    final var game = new NewGameBuilder().build(rows, columns, mines, 1L);

    assertEquals(rows, game.rows());
    assertEquals(columns, game.columns());
    assertEquals(mines, game.mines());

    assertEquals(mines, game.position().stream().filter(p -> p.hasMine()).count());
  }

  @Test
  void when_numberOfMinesIs50AndPositionsAre10000_Then_Success() {
    final var rows = 100;
    final var columns = 100;
    final var mines = 50;

    executeAndAssert(rows, columns, mines);
  }

  @Test
  void when_numberOfMinesIsBiggerThanOfPositions_Then_IllegalArgumentException() {
    final var rows = 4;
    final var columns = 4;
    final var mines = rows * columns + 1;

    assertThrows(
        IllegalArgumentException.class, () -> new NewGameBuilder().build(rows, columns, mines, 1L));
  }

  @Test
  void when_numberOfMinesIsLessThanPositions_Then_Success() {
    final var rows = 4;
    final var columns = 4;
    final var mines = 5;

    executeAndAssert(rows, columns, mines);
  }

  @Test
  void when_numberOfMinesIsOne_Then_Success() {
    final var rows = 4;
    final var columns = 4;
    final var mines = 1;

    executeAndAssert(rows, columns, mines);
  }

  @Test
  void when_numberOfMinesIsOneAndPositionIsOne_Then_Success() {
    final var rows = 1;
    final var columns = 1;
    final var mines = 1;

    executeAndAssert(rows, columns, mines);
  }

  @Test
  void when_numberOfMinesIsTheSameOfPositions_Then_Success() {
    final var rows = 4;
    final var columns = 4;
    final var mines = rows * columns;

    executeAndAssert(rows, columns, mines);
  }

  @Test
  void when_numberOfMinesIsZero_Then_Success() {
    final var rows = 4;
    final var columns = 4;
    final var mines = 0;

    executeAndAssert(rows, columns, mines);
  }

  @Test
  void when_numberOfMinesIsZeroAndPositionIsOne_Then_Success() {
    final var rows = 1;
    final var columns = 1;
    final var mines = 0;

    executeAndAssert(rows, columns, mines);
  }
}
