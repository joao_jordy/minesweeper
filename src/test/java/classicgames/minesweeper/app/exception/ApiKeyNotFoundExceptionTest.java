package classicgames.minesweeper.app.exception;

import static java.util.UUID.fromString;
import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;

class ApiKeyNotFoundExceptionTest {

  @Test
  void test() {
    final var exception =
        new ApiKeyNotFoundException(fromString("d0603a85-141f-4eeb-83ca-f88a1ba5c0d6"));

    assertEquals("d060****-****-****-****-******a5c0d6", exception.getMessage());
  }
}
