# README #

### What is this repository for? ###

* This is code challenge to develop the Minesweeper Classic Game using Java and Spring Boot within 3 days.
* Although this game seems to be simple, to achieve some results was necessary to use recursion.

### Useful Links ###

* [Application Endpoint](https://classicgames-minesweeper.herokuapp.com)
* [RESTFul API Documentation](https://app.swaggerhub.com/apis/joao_jordy/Minesweeper)
* [Basic User Interface](https://classicgames-minesweeper.herokuapp.com/api)
* [API Client in Golang](https://bitbucket.org/joao_jordy/minesweeper/src/master/client/go-client-generated.zip)

### Used Approaches ###

Due to the lack of time some approaches were used to improve productivity, like:

* API First, to define, document and generate both server side controllers and api client.
* Testing using SpringBootTest, calling endpoints directly.
* Persistency configuration using H2 database.
* Logging with Zalando's Logbook lib.

### Implementation Details ###

* This application is multi-user, so basically, game and move endpoints uses apikey information to filter contents.
* The authentication layer was made to be flexible, in order to implement multi-tenant behavior.
* Authentication expiration (apikey) occurs after one hour.

### Desirable points that were postponed for future versions ###

* Bitbucket Pipelines (Pipe) + Heroku, due to some kind of "bug" to start application. Deploy from heroku's cli are working well.
* Sonarcloud, to evaluate code coverage. Needs more configuration in order to compute test coverage.
* Frontend in html to play the game.

### Basic Steps to Start a Match ###

* Create User (POST /credential [username and password])
* Login (PUT /login [apikey])
* Create A Game (POST /game [apikey])
* Start The Game (PUT /game/{gameId}/start [apikey and gameUUID])

Then now is possible to turn positions, set flags an question marks.